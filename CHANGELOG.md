## 1.0.6 (2024-03-21)

### Added (4 changes)

- [format changelog](cross-solution/test-changelog-handling@0072cfd9587943ba8e9eb5dd2fbae6d393b51fc2)

- [(feature) test](cross-solution/test-changelog-handling@fe29855af5223b39a4bedf4dbc44c4d2fab79689)

- [add release](cross-solution/test-changelog-handling@3b3f83b4313cac4f8a67cf1c73cfba69668e7073)

- [add gitignore](cross-solution/test-changelog-handling@b89cf3b5105fc824a8b3467ea2480eadd56d2933)


### Changed (10 changes)

- [EADME](cross-solution/test-changelog-handling@20c94aeb491ea6c083635dd5b8c4301ab00f910b)

- [ttest](cross-solution/test-changelog-handling@2419fcb36ed19d988ec9c71fc24b90c8e0dd0e04)

- [implement feature #1](cross-solution/test-changelog-handling@66ca2f06d59895d3bf1051620c50ef255c43358d) ([merge request](cross-solution/test-changelog-handling!1))

- [(feature) uns dann ist da noch ein Feature](cross-solution/test-changelog-handling@79610cf4bd15c91ee6791f598f888fed19ee9b81)

- [(chore) test](cross-solution/test-changelog-handling@f69e66e63fcaed1f012075d258a3561b0d3bf974)

- [(test) test changelog creation](cross-solution/test-changelog-handling@4c45b5faa4a19826c4d2d61d49084548f583e9d0)

- [(test) test changelog generation](cross-solution/test-changelog-handling@0b21e6460716151095ae62cfe9d20d6f4655e395)

- [(chore) daily work](cross-solution/test-changelog-handling@a44112d407c41df11984e4b934fd4e92b21113ff)

- [(fix) fix gitlab-ci](cross-solution/test-changelog-handling@7ccd19740dc3bfe0017faaed1dc468b8ebe7affa)

- [(feature) just another change](cross-solution/test-changelog-handling@87078708a25814d282562d0469416b335a3d1cfd)


### change (1 change)

- [naother Change](cross-solution/test-changelog-handling@3aa4d5eabc5aefaadaa4b427fbec0023e10a16e9)

## 1.0.5 (2024-03-21)

# HUHU
### Added (3 changes)

- [(feature) test](cross-solution/test-changelog-handling@fe29855af5223b39a4bedf4dbc44c4d2fab79689)
- [add release](cross-solution/test-changelog-handling@3b3f83b4313cac4f8a67cf1c73cfba69668e7073)
- [add gitignore](cross-solution/test-changelog-handling@b89cf3b5105fc824a8b3467ea2480eadd56d2933)

# HUHU
### Changed (9 changes)

- [ttest](cross-solution/test-changelog-handling@2419fcb36ed19d988ec9c71fc24b90c8e0dd0e04)
- [implement feature #1](cross-solution/test-changelog-handling@66ca2f06d59895d3bf1051620c50ef255c43358d) ([merge request](cross-solution/test-changelog-handling!1))
- [(feature) uns dann ist da noch ein Feature](cross-solution/test-changelog-handling@79610cf4bd15c91ee6791f598f888fed19ee9b81)
- [(chore) test](cross-solution/test-changelog-handling@f69e66e63fcaed1f012075d258a3561b0d3bf974)
- [(test) test changelog creation](cross-solution/test-changelog-handling@4c45b5faa4a19826c4d2d61d49084548f583e9d0)
- [(test) test changelog generation](cross-solution/test-changelog-handling@0b21e6460716151095ae62cfe9d20d6f4655e395)
- [(chore) daily work](cross-solution/test-changelog-handling@a44112d407c41df11984e4b934fd4e92b21113ff)
- [(fix) fix gitlab-ci](cross-solution/test-changelog-handling@7ccd19740dc3bfe0017faaed1dc468b8ebe7affa)
- [(feature) just another change](cross-solution/test-changelog-handling@87078708a25814d282562d0469416b335a3d1cfd)

# HUHU
### change (1 change)

- [naother Change](cross-solution/test-changelog-handling@3aa4d5eabc5aefaadaa4b427fbec0023e10a16e9)
